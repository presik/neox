#!/bin/sh

sudo rm -R build/ dist/ neox.egg-info/
python3 setup.py sdist bdist_wheel

python setup.py sdist

twine upload dist/*
